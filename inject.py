import socket
import thread
import string
import select
import os
import sys
import ConfigParser

TAM_BUFFER = 65535
MAX_CLIENT_REQUEST_LENGTH = 8192 * 8

path = os.path.join(sys.path[0], 'Config.ini')
config = ConfigParser.ConfigParser()
config.read('%s' % (path)) 
LP = config.get('Injector Python','Listen_Port')
Payload = config.get('Injector Python','PAYLOAD')

Proxy = config.get('Injector Python','REMOTE_PROXY')

i = Proxy.find(':')
if i >= 0:
   ProxyHost,ProxyPort = (Proxy[:i], int(Proxy[i + 1:]))

def getReplacedPayload(payload, netData, hostPort, protocol):
	str = payload.replace('[netData]', netData)
	str = str.replace('[ssh]', (hostPort[0] + ':' + hostPort[1]))
	str = str.replace('[vpn]', (hostPort[0] + ':' + hostPort[1]))
	str = str.replace('[tcp]', (hostPort[0] + ':' + hostPort[1]))
	str = str.replace('[host_port]', (hostPort[0] + ':' + hostPort[1]))
	str = str.replace('[ip_port]', (hostPort[0] + ':' + hostPort[1]))
	str = str.replace('[ip:port]', (hostPort[0] + ':' + hostPort[1]))
	str = str.replace('[host:port]', (hostPort[0] + ':' + hostPort[1]))
	str = str.replace('[host]', hostPort[0])
	str = str.replace('[port]', hostPort[1])
	str = str.replace('[protocol]', protocol)
	str = str.replace('[method]', 'CONNECT')
	str = str.replace('[raw]', netData)
	str = str.replace('[real_raw]', netData + '\r\n\r\n')
	str = str.replace('[realData]', netData + '\r\n\r\n')
	str = str.replace('[real_host]', netData + '\r\n' + 'Host: [host]' + '\r\n\r\n')
	str = str.replace('[cr]', '\r')
	str = str.replace('[lf]', '\n')
	str = str.replace('[lfcr]', '\n\r')
	str = str.replace('[crlf]', '\r\n')
	return str

def getRequestProtocol(request):
	inicio = request.find(' ', request.find(':')) + 1
	str = request[inicio:]
	fim = str.find('\r\n')
	
	return str[:fim]


def getRequestHostPort(request):
	inicio = request.find(' ') + 1
	str = request[inicio:]
	fim = str.find(' ')
	
	hostPort = str[:fim]
	
	return hostPort.split(':')


def getRequestNetData(request):
	return request[:request.find('\r\n')]


def receiveHttpMsg(socket):
	len = 1
	
	data = socket.recv(1)
	while data.find('\r\n\r\n'.encode()) == -1:
		if not data: break
		data = data + socket.recv(1)
		len += 1
		if len > MAX_CLIENT_REQUEST_LENGTH: break
	
	return data
	

def doConnect(clientSocket, serverSocket, TAM_BUFFER):
	sockets = [clientSocket, serverSocket]
	timeout = 0
	print '[+] CONNECT started'
		
	while 1:
		timeout += 1
		ins, _, exs = select.select(sockets, [], sockets, 3)
		if exs: break
		
		if ins:
			for socket in ins:
				try:
					data = socket.recv(TAM_BUFFER)
					if not data: break;
					
					if socket is serverSocket:
						clientSocket.sendall(data)
					else:
						serverSocket.sendall(data)

					timeout = 0
				except:
					break

		if timeout == 60: break
	

def acceptThread(clientSocket, clientAddr):
	print '[+] Client connected: ', clientAddr
	

	request = receiveHttpMsg(clientSocket)
	

	if not request.startswith('CONNECT'):
		print '<!> Client request method != CONNECT!'
		clientSocket.sendall('HTTP/1.1 405 Only_CONNECT_Method!\r\n\r\n')
		clientSocket.close()
		thread.exit()


	netData = getRequestNetData(request)
	protocol = getRequestProtocol(request)
	hostPort = getRequestHostPort(netData)


	finalRequest = getReplacedPayload(Payload, netData, hostPort, protocol)


	proxySocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	proxySocket.connect((ProxyHost,ProxyPort))
	proxySocket.sendall(finalRequest)
	

	proxyResponse = receiveHttpMsg(proxySocket)
	
	print '[+] Status line: ' + getRequestNetData(proxyResponse)
	

	clientSocket.sendall(proxyResponse)
	

	if proxyResponse.find('200 ') != -1:
		doConnect(clientSocket, proxySocket, TAM_BUFFER)
	

	print '[+] Client ended    : ', clientAddr
	proxySocket.close()
	clientSocket.close()
	thread.exit()


print '################__Injector Python__###################'
print '\n'
print '<=> Injector.py'
print '<=> Listening   : ' + '127.0.0.1' + ':' + str(LP)
print '<=> Remote proxy: ' + Proxy
print '<=> Payload     : ' + Payload
print '\n'
print '######################################################'

server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server.bind(('127.0.0.1', int(LP)))
server.listen(1)

print '[+] Server listening... '


while True:
	clientSocket, clientAddr = server.accept()
	thread.start_new_thread(acceptThread, tuple([clientSocket, clientAddr]))

server.close()
